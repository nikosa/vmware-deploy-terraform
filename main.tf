variable "vsphere_user" {
  description	= "The vsphere user"
}

variable "vsphere_password" {
  description	= "The vsphere user account password"
}

variable "vsphere_server" {
  description	= "The vsphere server"
}

variable "datacenter" {
  default	= "testdc01"
}

variable "compute_cluster" {
  default	= "testcluster01"
}

resource "vsphere_license" "esxi-license" {
  license_key = ""
}

resource "vsphere_license" "vcsa-license" {
  license_key = ""
}

provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

resource "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

resource "vsphere_compute_cluster" "compute_cluster" {
  name          = var.compute_cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "compute_cluster" {
  name          = var.compute_cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}
